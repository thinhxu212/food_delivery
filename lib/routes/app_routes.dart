class Routes{
    static const splashScreen = '/';
    static const loginScreen = '/login';
    static const forgotPassword = '/forgot_password';
    static const homeBottom = '/home_bottom';
    static const profileDetail = '/profile_detail';
    static const changeProfile = '/change_profile';
}