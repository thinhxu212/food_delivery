import 'package:get/get.dart';
import 'package:ngon_ngu_kich_ban/routes/app_routes.dart';
import 'package:ngon_ngu_kich_ban/view/drawer/drawer_screen.dart';
import 'package:ngon_ngu_kich_ban/view/home/home_bottom.dart';
import 'package:ngon_ngu_kich_ban/view/home/home_screen/home_screen.dart';
import 'package:ngon_ngu_kich_ban/view/login/forgot_password.dart';
import 'package:ngon_ngu_kich_ban/view/login/login_screen.dart';
import 'package:ngon_ngu_kich_ban/view/login/login_view.dart';
import 'package:ngon_ngu_kich_ban/view/profile_detail/change_profile.dart';
import 'package:ngon_ngu_kich_ban/view/profile_detail/profile_detail.dart';

import 'package:ngon_ngu_kich_ban/view/splash/splash.dart';

class AppPages{
  static final pages = [
    GetPage(name: Routes.splashScreen, page: () => SplashScreen(),),
    GetPage(name: Routes.loginScreen, page: () => Login(),),
    GetPage(name: Routes.forgotPassword, page: () => ForgotPassword(),),
    GetPage(name: Routes.homeBottom, page: () => BottomHome(),),
    GetPage(name: Routes.profileDetail, page: () => ProfileDetail(),),
    GetPage(name: Routes.changeProfile, page: () => ChangeProfile()),
  ];
}