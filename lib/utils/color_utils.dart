import 'package:flutter/material.dart';

class ColorUtils {
  //color background
  static const COLOR_BG_SCREEN = Color(0xFFFF460A);
  static const COLOR_BG_SCREEN_1 = Color(0xFFF2F2F2);
  static const COLOR_BG_SCREEN_2 = Color(0xFFF5F5F8);
  static const COLOR_BG_SCREEN_3 = Color(0xFFFF4B3A);
  static const COLOR_BG_SCREEN_SEARCH = Color(0xFFEFEEEE);

  //color text
  static const COLOR_TEXT = Color(0xFFFA4A0C);
  static const COLOR_TEXT_1 = Color(0xFFF47B0A);
  static const COLOR_TEXT_2 = Color(0xFFF6F6F9);

  //color icon
  static const COLOR_ICON = Color(0xFFDF2C2C);
  static const COLOR_ICON_BOTTOM = Color(0xFFADADAF);

  //color background container
  static const COLOR_BG_C = Color(0xFFEB4796);
  static const COLOR_BG_C_1 = Color(0xFF0038FF);

}