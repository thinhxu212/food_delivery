import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ngon_ngu_kich_ban/routes/app_routes.dart';
import 'package:ngon_ngu_kich_ban/utils/color_utils.dart';
import 'package:ngon_ngu_kich_ban/widget/button_custom.dart';

class ProfileDetail extends StatelessWidget {
  const ProfileDetail({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorUtils.COLOR_BG_SCREEN_2,
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 40),
        child: Column(
          children: [
            const SizedBox(height: 50,),
            Row(
                children: [
                  GestureDetector(
                    onTap: () => Get.back(),
                      child: const Icon(Icons.arrow_back_ios)
                  )
                ]
            ),
            const SizedBox(height: 25,),
            const Row(
                children: [
                  Text('My profile', style: TextStyle(color: Colors.black, fontSize: 34, fontWeight: FontWeight.w600),)
                ]
            ),
            const SizedBox(height: 25,),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Text('Personal details', style: TextStyle(color: Colors.black, fontWeight: FontWeight.w600, fontSize: 18),),
                GestureDetector(
                  onTap: () =>Get.toNamed(Routes.changeProfile),
                    child: const Text('change', style: TextStyle(fontSize: 15, color: ColorUtils.COLOR_TEXT),)
                )
              ],
            ),
            const SizedBox(height: 8,),
            Container(
              height: 200,
              width: 315,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                color: Colors.white,
              ),
              child: Padding(
                padding: const EdgeInsets.only(left: 16, top: 18, right: 18,),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      width: 60,
                      height: 60,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: Colors.red
                      ),
                    ),
                    const SizedBox(width: 15,),
                    Expanded(
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const Text('Say my fucken name', style: TextStyle(fontWeight: FontWeight.w600,fontSize: 18),),
                            const SizedBox(height: 8,),
                            Text('abc@gmail.com', style: TextStyle(fontSize: 15, color: Colors.black.withOpacity(0.5)),),
                            Divider(color: Colors.black.withOpacity(0.5), ),
                            Text('+84123334566', style: TextStyle(fontSize: 15, color: Colors.black.withOpacity(0.5)),),
                            Divider(color: Colors.black.withOpacity(0.5), ),
                            Text('No 15 uti street off ovie palace road effurun delta state',style: TextStyle(fontSize: 15, color: Colors.black.withOpacity(0.5)) )
                          ]
                      ),
                    ),
                  ],
                ),
              ),
            ),
            const SizedBox(height: 22,),
            Container(
              width: 315,
              height: 60,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                color: Colors.white
              ),
              child: const Padding(
                padding: EdgeInsets.only(left: 20, right: 18),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text('Order', style: TextStyle(fontWeight: FontWeight.w600, fontSize: 18, color: Colors.black),),
                    Icon(Icons.arrow_forward_ios, size: 24, color: Colors.black,)
                  ],
                ),
              ),
            ),
            const SizedBox(height: 22,),
            Container(
              width: 315,
              height: 60,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  color: Colors.white
              ),
              child: const Padding(
                padding: EdgeInsets.only(left: 20, right: 18),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text('Pending reviews', style: TextStyle(fontWeight: FontWeight.w600, fontSize: 18, color: Colors.black),),
                    Icon(Icons.arrow_forward_ios, size: 24, color: Colors.black,)
                  ],
                ),
              ),
            ),
            const SizedBox(height: 22,),
            Container(
              width: 315,
              height: 60,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  color: Colors.white
              ),
              child: const Padding(
                padding: EdgeInsets.only(left: 20, right: 18),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text('Faq', style: TextStyle(fontWeight: FontWeight.w600, fontSize: 18, color: Colors.black),),
                    Icon(Icons.arrow_forward_ios, size: 24, color: Colors.black,)
                  ],
                ),
              ),
            ),
            const SizedBox(height: 22,),
            Container(
              width: 315,
              height: 60,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  color: Colors.white
              ),
              child: const Padding(
                padding: EdgeInsets.only(left: 20, right: 18),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text('Help', style: TextStyle(fontWeight: FontWeight.w600, fontSize: 18, color: Colors.black),),
                    Icon(Icons.arrow_forward_ios, size: 24, color: Colors.black,)
                  ],
                ),
              ),
            ),
            const SizedBox(height: 45,),
            const ButtonCustom(
                textContent: 'Update', style: TextStyle(fontSize: 17, fontWeight: FontWeight.w600, color: ColorUtils.COLOR_TEXT_2),
              color: ColorUtils.COLOR_TEXT,
              width: 315,
              height: 70,
              borderRadius: 30,
            )
          ],
        ),
      ),
    );
  }
}
