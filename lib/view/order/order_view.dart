import 'package:flutter/material.dart';
import 'package:ngon_ngu_kich_ban/utils/color_utils.dart';
import 'package:ngon_ngu_kich_ban/widget/button_custom.dart';

class Order extends StatelessWidget {
  const Order({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          const SizedBox(height: 60,),
          const Center(
              child: Text('History', style: TextStyle(fontSize: 18, fontWeight: FontWeight.w600, color: Colors.black),)
          ),
          const SizedBox(height: 170,),
          Image.asset('assets/images/shopping-cart.png'),
          const SizedBox(height: 25,),
          const Text('No orders yet', style: TextStyle(fontSize: 28, fontWeight: FontWeight.w600),),
          const SizedBox(height: 25,),
          Text('Hit the orange button down\n  below to Create an order', style: TextStyle(fontSize: 17, color: Colors.black.withOpacity(0.57)),),
          const SizedBox(height: 200,),
          const ButtonCustom(
            textContent: 'Start ordering',
            color: ColorUtils.COLOR_TEXT,
            height: 70,
            width: 314,
            borderRadius: 30,
            style: TextStyle(fontSize: 17, fontWeight: FontWeight.w600, color: Colors.white),
          )
        ],
      ),
    );
  }
}
