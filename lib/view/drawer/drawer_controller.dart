import 'package:get/get.dart';

class DrawerMenuController extends GetxController{
  bool isDrawerOpen = false;

  void toggleDrawer(){
    isDrawerOpen = !isDrawerOpen;
    update();
  }

  int selectedIndex = 0;

  void toggle(int index) {
    selectedIndex = index;
  }

  DrawerBar? groupDrawer;

  List<DrawerBar> drawerbar = [
    DrawerBar(icon: 'assets/icons/profile_icon.png', name: 'Profile'),
    DrawerBar(icon: 'assets/icons/orders_icon.png', name: 'Orders'),
    DrawerBar(icon: 'assets/icons/offer_icon.png', name: 'Offer and promo'),
    DrawerBar(icon: 'assets/icons/privacy_icon.png', name: 'Privacy policy'),
    DrawerBar(icon: 'assets/icons/security_icon.png', name: 'Security'),
  ];

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    groupDrawer = drawerbar[0];
  }
}

class DrawerBar {
  String? icon;
  String? name;

  DrawerBar({this.icon, this.name});
}