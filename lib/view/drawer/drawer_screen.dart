import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ngon_ngu_kich_ban/utils/color_utils.dart';
import 'package:ngon_ngu_kich_ban/view/drawer/drawer_controller.dart';

class DrawerScreen extends StatelessWidget {
  DrawerScreen({super.key});

  final DrawerMenuController drawerMenuController = Get.put(DrawerMenuController());

  @override
  Widget build(BuildContext context) {
    return Scaffold (
      body: GetBuilder<DrawerMenuController>(
        builder: (drawerMenuController){
          return ListView.builder(
            itemCount: drawerMenuController.drawerbar.length,
            itemBuilder: (context, index) => Drawer(
              backgroundColor: ColorUtils.COLOR_TEXT_1,
              child: Row(
                children: [
                  Image.asset(drawerMenuController.drawerbar[index].icon!),
                  Text(drawerMenuController.drawerbar[index].name!),
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
