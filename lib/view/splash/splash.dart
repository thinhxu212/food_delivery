import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ngon_ngu_kich_ban/routes/app_routes.dart';
import 'package:ngon_ngu_kich_ban/utils/color_utils.dart';
import 'package:ngon_ngu_kich_ban/widget/button_custom.dart';

class SplashScreen extends StatelessWidget {
  const SplashScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorUtils.COLOR_BG_SCREEN,
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Padding(
              padding: const EdgeInsets.only(right: 200),
              child: CircleAvatar(
                backgroundColor: Colors.white,
                radius: 36.5,
                child: Center(
                  child: Image.asset('assets/images/logo.png'),
                ),
              ),
            ),
            const Text('Food for \nEveryone',
            style: TextStyle(
              fontSize: 65,
              fontWeight: FontWeight.w800,
              color: Colors.white,
            ),
            ),
            Image.asset('assets/images/food.jpg'),
            ButtonCustom(
              textContent: 'Get Started',
              style: const TextStyle(fontSize: 17, fontWeight: FontWeight.w600, color: ColorUtils.COLOR_BG_SCREEN),
              width: 314,
              height: 70,
              borderRadius: 30,
              color: Colors.white,
              onTap: () => Get.offNamed(Routes.loginScreen),
            )
          ],
        ),
      ),
    );
  }
}
