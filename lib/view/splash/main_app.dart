import 'package:flutter/material.dart';
import 'package:flutter_zoom_drawer/flutter_zoom_drawer.dart';
import 'package:get/get.dart';
import 'package:ngon_ngu_kich_ban/view/home/home_bottom.dart';
import 'package:ngon_ngu_kich_ban/view/home/home_screen/home_screen.dart';
import 'package:ngon_ngu_kich_ban/view/home/home_screen/home_screen_controller.dart';
import 'package:ngon_ngu_kich_ban/view/menu/menu.dart';

class MainApp extends StatelessWidget {
  MainApp({super.key});

  final HomeScreenController homeScreenController = Get.put(HomeScreenController());

  @override
  Widget build(BuildContext context) {
    return GetBuilder<HomeScreenController>(
      builder: (controller) => ZoomDrawer(
        menuScreen: MenuScreen(),
        mainScreen: BottomHome(),
        borderRadius: 24,
        showShadow: true,
        angle: -12,
        drawerShadowsBackgroundColor: Colors.grey,
        slideWidth: MediaQuery.of(context).size.width *0.65,
      ),
    );
  }
}