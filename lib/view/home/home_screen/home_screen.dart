import 'package:flutter/material.dart';
import 'package:flutter_zoom_drawer/flutter_zoom_drawer.dart';
import 'package:get/get.dart';
import 'package:ngon_ngu_kich_ban/routes/app_routes.dart';
import 'package:ngon_ngu_kich_ban/utils/color_utils.dart';
import 'package:ngon_ngu_kich_ban/view/home/home_controller.dart';
import 'package:ngon_ngu_kich_ban/view/home/home_screen/home_screen_controller.dart';




class Home extends StatelessWidget {
  Home({super.key});

  final HomeScreenController homeScreenController = Get.put(HomeScreenController());
  final HomeController homeController = Get.put(HomeController());


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorUtils.COLOR_BG_SCREEN_1,
      body: Column(
        children: [
          const SizedBox(height: 70,),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 45),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                IconButton(
                    onPressed: () => homeController.toggleDrawer,
                    icon: Icon(Icons.menu)
                ),
                Image.asset('assets/icons/shopping-cart-logo.png')
              ],
            ),
          ),
          const SizedBox(height: 40,),
          const Padding(
            padding: EdgeInsets.only(left: 45),
              child: Row(
                children: [
                  Text('Delicious \nfood for you',
                    style: TextStyle(
                        fontSize: 34,
                        fontWeight: FontWeight.w700,
                        color: Colors.black),
                  ),
                ],
              )
          ),
          const SizedBox(height: 40,),
          Padding(
              padding: const EdgeInsets.symmetric(horizontal: 45),
            child: Container(
              decoration: BoxDecoration(
                color: ColorUtils.COLOR_BG_SCREEN_SEARCH,
                borderRadius: BorderRadius.circular(30)
              ),
              child: TextField(
                decoration: InputDecoration(
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(30),
                    borderSide: BorderSide.none
                  ),
                  hintText: 'Search',
                  hintStyle: TextStyle(color: Colors.black.withOpacity(0.5), fontSize: 17, fontWeight: FontWeight.w600),
                  prefixIcon: const Icon(Icons.search, size: 25, color: Colors.black,),
                ),
              ),
            ),
          ),
          const SizedBox(height: 30,),
          Padding(
            padding: const EdgeInsets.only(left: 45),
            child: TabBar(
              isScrollable: true,
                dividerColor: Colors.transparent,
                indicatorColor: ColorUtils.COLOR_TEXT,
                indicatorSize: TabBarIndicatorSize.label,
                labelColor: ColorUtils.COLOR_TEXT,
                labelStyle: const TextStyle(fontSize: 18,),
                unselectedLabelColor: Colors.grey,
                controller: homeScreenController.tabHomeController,
                onTap: (value) =>  homeScreenController.setTabIndex(value),
                tabs: const [
                  Tab(
                    child: Text(
                      'Foods',
                    ),
                  ),
                  Tab(
                    child: Text(
                      'Drinks',
                    ),
                  ),
                  Tab(
                    child: Text(
                      'Snacks',
                    ),
                  ),
                  Tab(
                    child: Text(
                      'Sauce',
                    ),
                  ),
                ]
            ),
          )
        ],
      ),
    );
  }
}
