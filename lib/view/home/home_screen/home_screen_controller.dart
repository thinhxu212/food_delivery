import 'package:flutter/material.dart';
import 'package:flutter_zoom_drawer/flutter_zoom_drawer.dart';
import 'package:get/get.dart';
import 'package:ngon_ngu_kich_ban/view/home/home_controller.dart';

class HomeScreenController extends GetxController with GetTickerProviderStateMixin{

  TabController? tabHomeController;
  int indexHomeTab = 0;

  @override
  void onInit() {
    super.onInit();
    tabHomeController = TabController(length: 4, vsync: this);
  }


  @override
  void dispose() {
    super.dispose();
    tabHomeController!.dispose();
  }

  void setTabIndex(int value){
    indexHomeTab = value;
  }


}