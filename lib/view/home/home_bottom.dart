import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ngon_ngu_kich_ban/utils/color_utils.dart';
import 'package:ngon_ngu_kich_ban/view/home/favorite/favorite.dart';
import 'package:ngon_ngu_kich_ban/view/home/history/history.dart';
import 'package:ngon_ngu_kich_ban/view/home/home_controller.dart';
import 'package:ngon_ngu_kich_ban/view/home/home_screen/home_screen.dart';
import 'package:ngon_ngu_kich_ban/view/home/home_screen/home_screen_controller.dart';
import 'package:ngon_ngu_kich_ban/view/home/profile/profile.dart';

class BottomHome extends StatelessWidget {
  BottomHome({super.key});

  final HomeController homeController = Get.put(HomeController());

  @override
  Widget build(BuildContext context) {
    return GetBuilder<HomeController>(
      builder: (homeController) => Scaffold(
        body:PageView(
          controller: homeController.pageController,
          children: [
            Home(),
            Favorite(),
            Profile(),
            History(),
          ],
        ),
        bottomNavigationBar: BottomNavigationBar(
          backgroundColor: Colors.pink,
          currentIndex: homeController.currentIndex,
          onTap: (index) {
            homeController.setCurrentIndex(index);
            homeController.jumToPage(index);
          },
          items: const [
            BottomNavigationBarItem(
              icon: Icon(
                Icons.home,
                color: ColorUtils.COLOR_ICON_BOTTOM,
                size: 32,
              ),
              activeIcon: Icon(
                Icons.home,
                color: ColorUtils.COLOR_TEXT,
                size: 32,
              ),
              label: '',
            ),
            BottomNavigationBarItem(
              icon: Icon(
                Icons.favorite_border,
                color: ColorUtils.COLOR_ICON_BOTTOM,
                size: 32,
              ),
              activeIcon: Icon(
                Icons.favorite_border,
                color: ColorUtils.COLOR_TEXT,
                size: 32,
              ),
              label: '',
            ),
            BottomNavigationBarItem(
              icon: Icon(
                Icons.person_outline,
                color: ColorUtils.COLOR_ICON_BOTTOM,
                size: 32,
              ),
              activeIcon: Icon(
                Icons.person_outline,
                color: ColorUtils.COLOR_TEXT,
                size: 32,
              ),
              label: '',
            ),
            BottomNavigationBarItem(
              icon: Icon(
                Icons.history,
                color: ColorUtils.COLOR_ICON_BOTTOM,
                size: 32,
              ),
              activeIcon: Icon(
                Icons.history,
                color: ColorUtils.COLOR_TEXT,
                size: 32,
              ),
              label: '',
            ),
          ],
        ),
      ),
    );
  }
}
