import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ngon_ngu_kich_ban/routes/app_routes.dart';
import 'package:ngon_ngu_kich_ban/utils/color_utils.dart';
import 'package:ngon_ngu_kich_ban/view/home/profile/profile_controller.dart';
import 'package:ngon_ngu_kich_ban/widget/button_custom.dart';

class Profile extends StatelessWidget {
  Profile({super.key});

  final ProfileController profileController = Get.put(ProfileController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorUtils.COLOR_BG_SCREEN_2,
      body: GetBuilder<ProfileController>(
        builder: (controller) =>  Column(
          children: [
            const SizedBox(height: 60),
            const Center(
              child: Text('My profile', style: TextStyle(fontSize: 18, fontWeight: FontWeight.w600),),
            ),
            const SizedBox(height: 70,),
            const Row(
                children: [
                  SizedBox(width: 50,),
                  Text('Information',style: TextStyle(fontSize: 17, fontWeight: FontWeight.w600),)
                ]
            ),
            const SizedBox(height: 10,),
            GestureDetector(
              onTap: () => Get.toNamed(Routes.profileDetail),
              child: Container(
               width: 315,
                height: 133,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(20),
                ),
                child: Padding(
                  padding: const EdgeInsets.only(left: 10, top: 12, right: 14, bottom: 14),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                            width: 60,
                            height: 60,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              color: Colors.red
                            ),
                      ),
                  const SizedBox(width: 10,),
                  Expanded(
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                const Text('Say my fucken name', style: TextStyle(fontWeight: FontWeight.w600,fontSize: 18),),
                                const SizedBox(height: 8,),
                                Text('abc@gmail.com', style: TextStyle(fontSize: 13, color: Colors.black.withOpacity(0.5)),),
                                const SizedBox(height: 7,),
                                Text('No 15 uti street off ovie palace road effurun delta state',style: TextStyle(fontSize: 13, color: Colors.black.withOpacity(0.5)) )
                              ]
                          ),
                  ),
                    ],
                  ),
                ),
              ),
            ),
            const SizedBox(height: 30,),
            const Row(
                children: [
                  SizedBox(width: 50,),
                  Text('Payment Method',style: TextStyle(fontSize: 17, fontWeight: FontWeight.w600))
                ]
            ),
            const SizedBox(height: 15,),
            Container(
              width: 315,
              height: 231,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(20)
              ),
              child: ListView.builder(
                physics: const NeverScrollableScrollPhysics(),
                 itemCount: profileController.payments.length,
                  itemBuilder: (context, index) => RadioListTile<PaymentMethod>(
                    activeColor: ColorUtils.COLOR_TEXT,
                    value: profileController.payments[index],
                    groupValue: profileController.groupItem,
                    onChanged: (PaymentMethod? payments) {
                     profileController.setChangePayment(payments);
                    },
                    title: Row(
                        children: [
                          Container(
                            width: 40,
                            height: 40,
                            decoration: BoxDecoration(
                              color: profileController.payments[index].color,
                              borderRadius: BorderRadius.circular(10),
                            ),
                            child: Image.asset(profileController.payments[index].icon!),
                          ),
                          const SizedBox(width: 10,),
                          Text(profileController.payments[index].name!)
                        ]
                    ),
                  ),
              ),
            ),
            const SizedBox(height: 90,),
            const ButtonCustom(
              textContent: 'Update',
              color: ColorUtils.COLOR_TEXT,
              height: 70,
              width: 314,
              borderRadius: 30,
              style: TextStyle(fontSize: 17, fontWeight: FontWeight.w600, color: Colors.white),
            )
          ],
        ),
      ),
    );
  }
}
