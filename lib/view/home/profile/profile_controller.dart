import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ngon_ngu_kich_ban/utils/color_utils.dart';

class ProfileController extends GetxController {
  int selectedIndex = 0;

  void toggle(int index) {
    selectedIndex = index;
  }
  PaymentMethod? groupItem ;


  List<PaymentMethod> payments = [
    PaymentMethod(id: 1,icon: 'assets/icons/credit-icon.png', name: 'Card', color: ColorUtils.COLOR_TEXT_1),
    PaymentMethod(id: 2,icon: 'assets/icons/bank-icon.png', name: 'Bank account', color: ColorUtils.COLOR_BG_C),
    PaymentMethod(id: 3,icon: 'assets/icons/paypal-icon.png', name: 'Paypal', color: ColorUtils.COLOR_BG_C_1),
  ];

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    groupItem = payments[0];
  }
  void setChangePayment(PaymentMethod? value){
    groupItem = value;
    update();
  }
}

class PaymentMethod {
  int? id;
  String? icon;
  String? name;
  Color? color;

  PaymentMethod({this.id,this.icon, this.name, this.color});
}
