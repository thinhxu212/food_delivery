import 'package:flutter/material.dart';
import 'package:flutter_zoom_drawer/flutter_zoom_drawer.dart';
import 'package:get/get.dart';
import 'package:ngon_ngu_kich_ban/view/home/favorite/favorite.dart';
import 'package:ngon_ngu_kich_ban/view/home/history/history.dart';
import 'package:ngon_ngu_kich_ban/view/home/home_screen/home_screen.dart';
import 'package:ngon_ngu_kich_ban/view/home/profile/profile.dart';

class HomeController extends GetxController {

  int currentIndex = 0;
  PageController pageController = PageController();

  final zoomDrawerController = ZoomDrawerController();

  void toggleDrawer() {
    print("Toggle drawer");
    zoomDrawerController.toggle?.call();
    update();
  }

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    pageController.dispose();
  }

  void jumToPage(int value){
    pageController.jumpToPage(value);
    update();
  }

  void setCurrentIndex(int value){
    currentIndex = value;
    update();
  }

}