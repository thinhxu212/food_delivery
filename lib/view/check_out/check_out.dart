import 'package:flutter/material.dart';
import 'package:ngon_ngu_kich_ban/utils/color_utils.dart';

class CheckOut extends StatelessWidget {
  const CheckOut({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorUtils.COLOR_BG_SCREEN_2,
      body: Column(
        children: [
          const Row(
            children: [
              Icon(Icons.arrow_back_ios),
              Text('Check Out', style: TextStyle(fontWeight: FontWeight.w600, fontSize: 18),)
            ],
          ),
          const Text('Delivery', style: TextStyle(fontSize: 34, fontWeight: FontWeight.w600),),
          const Row(
              children: [
                Text('Address detail', style: TextStyle(fontSize: 17, fontWeight: FontWeight.w600),),
                Text('change', style: TextStyle(fontSize: 15, color: ColorUtils.COLOR_TEXT_1),)
              ]
          ),
          Container(
            width: 315,
            height: 150,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              color: Colors.white,
            ),
            child: const Padding(
              padding: EdgeInsets.only(left: 15, top: 15, right: 20),
              child: Column(
                children: [
                  Text('Name', style: TextStyle(fontSize: 17, fontWeight: FontWeight.w500),),
                  Divider(),
                  Text('address', style: TextStyle(fontSize: 15),),
                  Divider(),
                  Text('number', style: TextStyle(fontSize: 15),)
                ],
              ),
            ),
          ),
          Text('Delivery Method', style: TextStyle(fontSize: 17, fontWeight: FontWeight.w600),),
          Container(
            width: 315,
            height: 150,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(20)
            ),
            //child: ,
          )
        ],
      ),
    );
  }
}
