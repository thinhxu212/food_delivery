import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ngon_ngu_kich_ban/utils/color_utils.dart';
import 'package:ngon_ngu_kich_ban/view/login/login_controller.dart';
import 'package:ngon_ngu_kich_ban/view/login/login_view.dart';
import 'package:ngon_ngu_kich_ban/view/login/sign_up_view.dart';

class Login extends StatelessWidget {
  Login({super.key});
  final LoginController loginController = Get.put(LoginController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorUtils.COLOR_BG_SCREEN_1,
      body: GetBuilder<LoginController>(
        builder: (loginController) => SingleChildScrollView(
          child: Column(
            children: [
              Container(
                width: Get.width,
                height: Get.height*0.35,
                decoration: const BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(bottomRight: Radius.circular(30), bottomLeft: Radius.circular(30)),
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Image.asset('assets/images/logo.png'),
                    Container(
                      decoration: const BoxDecoration(
                          borderRadius: BorderRadius.only(bottomRight: Radius.circular(30), bottomLeft: Radius.circular(30))
                      ),
                      child: TabBar(
                        dividerColor: Colors.transparent,
                        indicatorColor: ColorUtils.COLOR_TEXT,
                        indicatorSize: TabBarIndicatorSize.label,
                        labelColor: Colors.black,
                        labelStyle: const TextStyle(fontSize: 18, fontWeight: FontWeight.w600),
                        unselectedLabelColor: Colors.grey,
                        controller: loginController.tabController,
                          onTap: (value) =>  loginController.setTabIndex(value),
                          tabs: const [
                            Tab(
                              child: Text(
                                'Login',
                              ),
                            ),
                            Tab(
                              child: Text(
                                'Sign up',
                              ),
                            ),
                          ]
                      ),
                    )
                  ],
                ),
              ),
              Container(
                height: Get.height-Get.height*0.35,
                child: TabBarView(
                  controller: loginController.tabController,
                  children: [
                    Container(
                      child: LoginView(),
                    ),
                    Container(
                      child: SignUpView(),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
