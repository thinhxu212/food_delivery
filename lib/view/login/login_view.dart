import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ngon_ngu_kich_ban/routes/app_routes.dart';
import 'package:ngon_ngu_kich_ban/utils/color_utils.dart';
import 'package:ngon_ngu_kich_ban/view/login/login_controller.dart';
import 'package:ngon_ngu_kich_ban/widget/button_custom.dart';

class LoginView extends StatelessWidget {
  LoginView({super.key});
  final LoginController loginController = Get.put(LoginController());


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorUtils.COLOR_BG_SCREEN_1,
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 50),
        child: Column(
          children: [
            const SizedBox(height: 50,),
            TextFormField(
              controller: loginController.emailController,
              cursorColor: Colors.black,
              style: const TextStyle(color: Colors.black, fontSize: 17, fontWeight: FontWeight.w600),
              decoration: InputDecoration(
                contentPadding: const EdgeInsets.symmetric(vertical: 20),
                labelText: 'Email Address',
                labelStyle: TextStyle(color: Colors.black.withOpacity(0.4), fontSize: 15, fontWeight: FontWeight.w600),

              ),
            ),
            const SizedBox(height: 40,),
            TextFormField(
              controller: loginController.passwordController,
              cursorColor: Colors.black,
              obscureText: true,
              obscuringCharacter: '*',
              decoration: InputDecoration(
                contentPadding: const EdgeInsets.symmetric(vertical: 20),
                labelText: 'Password',
                labelStyle: TextStyle(color: Colors.black.withOpacity(0.4), fontSize: 15, fontWeight: FontWeight.w600),
              ),
            ),
            const SizedBox(height: 40,),
            InkWell(
              onTap: () => Get.toNamed(Routes.forgotPassword),
              child: const Padding(
                padding: EdgeInsets.only(right: 150),
                  child: Text('Forgot passcode?',
                    style: TextStyle(
                        fontSize: 17,
                        fontWeight: FontWeight.w600,
                        color: ColorUtils.COLOR_TEXT),
                  )
              ),
            ),
            const SizedBox(height: 80,),
            ButtonCustom(
              textContent: 'Login',
              color: ColorUtils.COLOR_TEXT,
              height: 70,
              width: 314,
              borderRadius: 30,
              style: const TextStyle(fontSize: 17, fontWeight: FontWeight.w600, color: Colors.white),
              onTap: () => Get.toNamed(Routes.homeBottom),
            )

          ],
        ),
      ),
    );
  }
}
