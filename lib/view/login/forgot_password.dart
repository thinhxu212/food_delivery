import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ngon_ngu_kich_ban/utils/color_utils.dart';
import 'package:ngon_ngu_kich_ban/view/login/login_controller.dart';
import 'package:ngon_ngu_kich_ban/widget/button_custom.dart';

class ForgotPassword extends StatelessWidget {
  ForgotPassword({super.key});
  final LoginController loginController = Get.put(LoginController());


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //backgroundColor: ColorUtils.COLOR_BG_SCREEN_1,
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 50),
        child: Column(
          //mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            SizedBox(height: 50,),
            Image.asset('assets/images/logo.png'),
            const SizedBox(height: 50,),
            TextFormField(
              controller: loginController.newemailController,
              cursorColor: Colors.black,
              style: const TextStyle(color: Colors.black, fontSize: 17, fontWeight: FontWeight.w600),
              decoration: InputDecoration(
                contentPadding: const EdgeInsets.symmetric(vertical: 20),
                labelText: 'Email Address',
                labelStyle: TextStyle(color: Colors.black.withOpacity(0.4), fontSize: 15, fontWeight: FontWeight.w600),

              ),
            ),
            const SizedBox(height: 20,),
            TextFormField(
              controller: loginController.newpasswordController,
              cursorColor: Colors.black,
              obscureText: true,
              obscuringCharacter: '*',
              decoration: InputDecoration(
                contentPadding: const EdgeInsets.symmetric(vertical: 20),
                labelText: 'New Password',
                labelStyle: TextStyle(color: Colors.black.withOpacity(0.4), fontSize: 15, fontWeight: FontWeight.w600),
              ),
            ),
            const SizedBox(height: 20,),
            TextFormField(
              controller: loginController.repasswordController,
              cursorColor: Colors.black,
              obscureText: true,
              obscuringCharacter: '*',
              decoration: InputDecoration(
                contentPadding: const EdgeInsets.symmetric(vertical: 20),
                labelText: 'Re-Enter Password',
                labelStyle: TextStyle(color: Colors.black.withOpacity(0.4), fontSize: 15, fontWeight: FontWeight.w600),
              ),
            ),
            const SizedBox(height: 120,),
            GestureDetector(
              onTap: () => Get.back(),
              child: const ButtonCustom(
                textContent: 'Confirm',
                color: ColorUtils.COLOR_TEXT,
                height: 70,
                width: 314,
                borderRadius: 30,
                style: TextStyle(fontSize: 17, fontWeight: FontWeight.w600, color: Colors.white),
              ),
            )
          ],
        ),
      ),
    );
  }
}

